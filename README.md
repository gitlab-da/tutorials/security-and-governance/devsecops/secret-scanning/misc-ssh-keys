# Misc SSH Keys

This project contains misc ssh-keys used for testing GitLab's secret scanning
capabilities.

The .gitlab-ci.yml file adds [Secret Detection](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/) to the CI/CD pipeline to detect vulnerabilities before they make it into production. The secret detection scanner uses the [SSH ruleset override project](https://gitlab.com/gitlab-da/tutorials/security-and-governance/devsecops/secret-scanning/ssh-ruleset-override) to remotely override the scanner's output for the detected SSH keys.

## References

- [Pipeline Secret Scanning Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/)

- [Custom Rulesets Documentation](https://docs.gitlab.com/ee/user/application_security/secret_detection/pipeline/#custom-rulesets)

- [Feature proposal: Ability to include remote "custom ruleset"](https://gitlab.com/gitlab-org/gitlab/-/issues/336395)

- Project photo by [Jozsef Hocza](https://unsplash.com/@hocza?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash) on [Unsplash](https://unsplash.com/photos/three-silver-keys-y5N2HDwagVw?utm_content=creditCopyText&utm_medium=referral&utm_source=unsplash)